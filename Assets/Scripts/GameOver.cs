﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{

    public void Exit()
    {
        Debug.Log("Exit");
        Application.Quit();
    }

    public void Play()
    {
        Debug.Log("Play");
        SceneManager.LoadScene("SpaceShooter");
    }
}
