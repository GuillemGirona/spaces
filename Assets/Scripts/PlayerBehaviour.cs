﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerBehaviour : MonoBehaviour {
	public float speed;
	private Vector2 axis;
	public Vector2 limits;
	public Propeller propeller;
	public ParticleSystem explosion;
	public GameObject graphics;
	private BoxCollider2D myCollider;
	public AudioSource audio;
    private bool canmove;
    public Weapons weapon;
    private bool parpadeo;


	void Awake(){
		myCollider = GetComponent<BoxCollider2D> ();
        canmove = true;

	}

	// Update is called once per frame
	void Update () {
        if (canmove == true) {
            transform.Translate(axis * speed * Time.deltaTime);

            if (transform.position.x > limits.x) {
                transform.position = new Vector3(limits.x, transform.position.y, transform.position.z);
            } else if (transform.position.x < -limits.x) {
                transform.position = new Vector3(-limits.x, transform.position.y, transform.position.z);
            }

            if (transform.position.y > limits.y) {
                transform.position = new Vector3(transform.position.x, limits.y, transform.position.z);
            } else if (transform.position.y < -limits.y) {
                transform.position = new Vector3(transform.position.x, -limits.y, transform.position.z);
            }

            if (axis.y > 0) {
                propeller.RedFire();
            } else if (axis.y < 0) {
                propeller.BlueFire();
            } else {
                propeller.Stop();
            }
        }
	}

	public void SetAxis(Vector2 currentAxis){
		axis = currentAxis;
	}

	public void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Meteor") {
			Explode ();
		}
	}

	private void Explode(){
		//Perdemos 1 vida
		MyGameManager.getInstance().LoseLive();
        canmove = false;
        weapon.disparar = false;
        //Quitar grafico
        graphics.SetActive(false);

		//Quitar Collider
		myCollider.enabled = false;

		//Lanzar Partícula
		explosion.Play();

		//Play Audio
		audio.Play();

        if (MyGameManager.getInstance().Dead()){
            Debug.Log("GameOver");
            SceneManager.LoadScene("GameOver",LoadSceneMode.Additive);

        }
		//A los 2 segundos Reset
		Invoke("Reset",2);
        InvokeRepeating("Parpadeo", 1, 0.1f);
	}

	private void Reset(){
        CancelInvoke("Parpadeo");
		graphics.SetActive(true);
		myCollider.enabled = true;
        canmove = true;
        weapon.disparar = true;
    }
    private void Parpadeo()
    {
        if (parpadeo)
        {
            graphics.SetActive(false);
            parpadeo = false;
        }
        else
        {
            graphics.SetActive(true);
            parpadeo = true;
        }
    }
}
